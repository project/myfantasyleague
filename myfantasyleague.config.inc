<?php

/**
 * @file
 * Configuration options for the MyFantasyLeague module.
 */

/**
 * Configuration options for the MyFantasyLeague module.
 */
function myfantasyleague_config() {
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Configure the MyFantasyLeague module.'),
  );
  $form['league_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Your MFL League ID'),
    '#default_value' => variable_get('myfantasyleague_league_id'),
    '#description' => t('To find your league ID, go to your league home page and look at the URL: http://www.myfantasyleague.com/2011/home/xxxxx. The \'xxxxx\' part is your league ID.'),
  );
  $form['year'] = array(
    '#type' => 'textfield',
    '#title' => t('The year of your MFL league'),
    '#default_value' => variable_get('myfantasyleague_year'),
    '#description' => '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  
  return $form;
}

/**
 * MyFantasyLeague configuration validator.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function myfantasyleague_config_validate($form, &$form_state) {
  if (!is_numeric($form['league_id']['#value'])) {
    form_error($form['league_id'], t('The League ID must be numeric!'));
  }
  if (!is_numeric($form['year']['#value'])) {
    form_error($form['year'], t('The year must be numeric!'));
  }
}

/**
 * MyFantasyLeague configuration submit handler.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function myfantasyleague_config_submit($form, &$form_state) {
  // set the form information into variables
  variable_set('myfantasyleague_league_id', $form['league_id']['#value']);
  variable_set('myfantasyleague_year', $form['year']['#value']);
  
  // clear all cached data from this module since league info might have changed
  cache_clear_all('myfantasyleague', 'cache', TRUE); 
  
  drupal_set_message(t('Saved changes.'));
}
